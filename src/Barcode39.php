<?php

namespace MINNIS\Barcode39;

/**
 * @see https://en.wikipedia.org/wiki/Code_39
 */
class Barcode39
{
    const IMAGE_PNG = 'png';
    const IMAGE_GIF = 'gif';
    const IMAGE_JPG = 'jpg';

    const b00 = '00';
    const b01 = '01';
    const b10 = '10';
    const b11 = '11';

    /**
     * Code 39 character matrix
     * @var array
     */
    private $code39matrix = array(
        ' ' => 100011011001110110,
        '$' => 100010001000100110,
        '%' => 100110001000100010,
        '*' => 100010011101110110,
        '+' => 100010011000100010,
        '-' => 100010011001110111,
        '.' => 110010011001110110,
        '/' => 100010001001100010,
        '0' => 100110001101110110,
        '1' => 110110001001100111,
        '2' => 100111001001100111,
        '3' => 110111001001100110,
        '4' => 100110001101100111,
        '5' => 110110001101100110,
        '6' => 100111001101100110,
        '7' => 100110001001110111,
        '8' => 110110001001110110,
        '9' => 100111001001110110,
        'A' => 110110011000100111,
        'B' => 100111011000100111,
        'C' => 110111011000100110,
        'D' => 100110011100100111,
        'E' => 110110011100100110,
        'F' => 100111011100100110,
        'G' => 100110011000110111,
        'H' => 110110011000110110,
        'I' => 100111011000110110,
        'J' => 100110011100110110,
        'K' => 110110011001100011,
        'L' => 100111011001100011,
        'M' => 110111011001100010,
        'N' => 100110011101100011,
        'O' => 110110011101100010,
        'P' => 100111011101100010,
        'Q' => 100110011001110011,
        'R' => 110110011001110010,
        'S' => 100111011001110010,
        'T' => 100110011101110010,
        'U' => 110010011001100111,
        'V' => 100011011001100111,
        'W' => 110011011001100110,
        'X' => 100010011101100111,
        'Y' => 110010011101100110,
        'Z' => 100011011101100110,
    );

    /**
     * Width scaling (thin bar = 1px * scale, thick bar = 3px * scale)
     * @var int
     */
    public $scaling = 1;

    /**
     * Barcode background color (RGB)
     * @var array
     */
    public $background_rgb = array(255, 255, 255);

    /**
     * Barcode height
     * @var int
     */
    public $barcode_height = 100;

    /**
     * Barcode padding
     * @var int
     */
    public $padding = 5;

    /**
     * Print code as text below barcode
     * @var bool $use_text
     */
    public $use_text = true;

    /**
     * Text font size (if use_text is enabled)
     * @var int
     */
    public $text_size = 4;

    /**
     * Adds start/stopsymbol * automaticly
     * @var bool
     */
    public $use_startstop_symbol = true;

    /**
     * Output start/stopsymbol * (if use_text is enabled)
     * @var bool
     */
    public $print_startstop_symbol = false;

    /**
     * @var string
     */
    private $inputstring;

    /**
     * @var array
     */
    private $code39array = array();

    /**
     * @param string $code
     */
    public function __construct($code)
    {
        $this->inputstring = $code;
    }


    /**
     * Displays barcode as imagefile
     * @param string|null $imagetype Defaults 'png'
     * @param string|null $filename (optional)
     * @return bool
     */
    public function draw($imagetype = self::IMAGE_PNG, $filename = null)
    {
        if (false === $this->isImageTypeAvailable($imagetype)) {
            return false;
        }

        $image = $this->generate();

        if ($image === false) {
            return false;
        }

        if (null !== $filename) {
            header('Content-Disposition: inline; filename="' . basename($filename) . '"');
        }

        switch ($imagetype) {

            case self::IMAGE_PNG:
                header('Content-Type: image/png');
                $state = imagepng($image);
                break;

            case self::IMAGE_GIF:
                header('Content-Type: image/gif');
                $state = imagegif($image);
                break;

            case self::IMAGE_JPG:
                header('Content-Type: image/jpeg');
                $state = imagejpeg($image);
                break;

            default:
                $state = false;
        }

        return $state;
    }

    /**
     * Writes barcode as file to server
     * @param string $imagetype
     * @param string $filename
     * @return bool
     */
    public function file($imagetype, $filename)
    {
        if (false === $this->isImageTypeAvailable($imagetype)) {
            return false;
        }

        $image = $this->generate();

        if ($image === false) {
            return false;
        }

        switch ($imagetype) {

            case self::IMAGE_PNG:
                $state = imagepng($image, $filename);
                break;

            case self::IMAGE_GIF:
                $state = imagegif($image, $filename);
                break;

            case self::IMAGE_JPG:
                $state = imagejpeg($image, $filename);
                break;

            default:
                $state = false;
        }

        return $state;
    }

    /**
     * Returns base64 encoded barcode (usecases: inline image email, store in database, send in json)
     * @param string|null $imagetype Defaults 'png'
     * @return bool
     */
    public function base64($imagetype = self::IMAGE_PNG)
    {
        if (false === $this->isImageTypeAvailable($imagetype)) {
            return false;
        }

        $image = $this->generate();

        if ($image === false) {
            return false;
        }

        ob_start();

        switch ($imagetype) {

            case self::IMAGE_PNG:
                $state = imagepng($image);
                break;

            case self::IMAGE_GIF:
                $state = imagegif($image);
                break;

            case self::IMAGE_JPG:
                $state = imagejpeg($image);
                break;

            default:
                $state = false;
        }

        $image_data = ob_get_clean();

        if ($state === false) {
            return false;
        }

        return base64_encode($image_data);
    }


    /**
     * @param string $code
     * @param string|null $filename
     * @return bool
     */
    public static function drawPNG($code, $filename = null)
    {
        $barcode39instance = new self($code);

        return $barcode39instance->draw(self::IMAGE_PNG, $filename);
    }

    /**
     * @param string $code
     * @param string|null $filename
     * @return bool
     */
    public static function drawGIF($code, $filename = null)
    {
        $barcode39instance = new self($code);

        return $barcode39instance->draw(self::IMAGE_GIF, $filename);
    }

    /**
     * @param string $code
     * @param string|null $filename
     * @return bool
     */

    public static function drawJPG($code, $filename = null)
    {
        $barcode39instance = new self($code);

        return $barcode39instance->draw(self::IMAGE_JPG, $filename);
    }

    /**
     * @param string $code
     * @param string $filename
     * @return bool
     */
    public static function filePNG($code, $filename)
    {
        $barcode39instance = new self($code);

        return $barcode39instance->file(self::IMAGE_PNG, $filename);
    }


    /**
     * @param string $code
     * @param string $filename
     * @return bool
     */
    public static function fileGIF($code, $filename)
    {
        $barcode39instance = new self($code);

        return $barcode39instance->file(self::IMAGE_GIF, $filename);
    }


    /**
     * @param string $code
     * @param string $filename
     * @return bool
     */
    public static function fileJPG($code, $filename)
    {
        $barcode39instance = new self($code);

        return $barcode39instance->file(self::IMAGE_JPG, $filename);
    }

    /**
     * @param string $code
     * @return string|false
     */
    public static function base64PNG($code)
    {
        $barcode39instance = new self($code);

        return $barcode39instance->base64(self::IMAGE_PNG);
    }

    /**
     * @param string $code
     * @return string|false
     */
    public static function base64GIF($code)
    {
        $barcode39instance = new self($code);

        return $barcode39instance->base64(self::IMAGE_GIF);
    }

    /**
     * @param string $code
     * @return string|false
     */
    public static function base64JPG($code)
    {
        $barcode39instance = new self($code);

        return $barcode39instance->base64(self::IMAGE_JPG);
    }

    /**
     * @param string $imagetype
     * @return bool
     */
    private function isImageTypeAvailable($imagetype)
    {
        switch ($imagetype) {

            case self::IMAGE_GIF:
                return function_exists('imagegif');
                break;

            case self::IMAGE_PNG:
                return function_exists('imagepng');
                break;

            case self::IMAGE_JPG:
                return function_exists('imagejpeg');
                break;

            default:
        }

        return false;
    }

    private function normalizeInputStringToCode39Array()
    {
        $codeCharacters = (string)strtoupper($this->inputstring);

        if ($this->use_startstop_symbol) {
            $codeCharacters = '*' . $codeCharacters . '*';
        }

        foreach (str_split($codeCharacters) as $codeCharacter) {

            // filter out for invalid code39 characters
            if (false === isset($this->code39matrix[$codeCharacter])) {
                continue;
            }

            $this->code39array[] = $codeCharacter;
        }
    }

    /**
     * @return resource|false
     */
    private function generate()
    {
        $this->normalizeInputStringToCode39Array();

        $bars = array();

        // set starting position pointer at padding offset
        $position = $this->padding;

        $i = 0;
        foreach ($this->code39array as $code39character) {

            // add separator between characters if not first character
            $code39sequence = ($i > 0 ? self::b01 : '') . $this->code39matrix[$code39character];

            $code39bars = str_split($code39sequence, 2);

            // add each bar coordinates and params
            foreach ($code39bars as $bar) {

                // bar color
                $fill = ($bar === self::b11 || $bar === self::b10) ? 'color_bar' : 'color_gap';

                // bar width
                $width = ($bar === self::b11 || $bar === self::b00) ? (3 * $this->scaling) : (1 * $this->scaling);

                // add bar coordinates
                $bars[] = array(
                    'position' => $position,
                    'width'    => $width,
                    'fill'     => $fill,
                );

                // move position pointer
                $position += $width;
            }

            $i++;
        }

        if (empty($bars)) {

            return false;
        }

        //right side padding
        $barcode_width = $position + $this->padding;

        // initialize image
        $image = imagecreate($barcode_width, $this->barcode_height);

        $system_colors = array(
            'color_bar'        => imagecolorallocate($image, 0, 0, 0),
            'color_gap'        => imagecolorallocate($image, 255, 255, 255),
            'color_background' => imagecolorallocate($image, $this->background_rgb[0], $this->background_rgb[1], $this->background_rgb[2]),
        );

        // fill background
        imagefilledrectangle($image, 0, 0, $barcode_width, $this->barcode_height, $system_colors['color_background']);

        // draw the bars
        foreach ($bars as $bar) {
            imagefilledrectangle(
                $image,
                $bar['position'],
                $this->padding,
                $bar['position'] - 1 + $bar['width'],
                $this->barcode_height - $this->padding - 1,
                $system_colors[$bar['fill']]
            );
        }

        // code below barcode
        if ($this->use_text === true) {

            $barcode_string = implode(' ', $this->code39array);

            if ($this->print_startstop_symbol === false) {
                $barcode_string = trim(str_replace('*', '', $barcode_string));
            }

            imagefilledrectangle(
                $image,
                $this->padding,
                ($this->barcode_height - ($this->padding * 2)) - 10,
                $barcode_width - $this->padding,
                $this->barcode_height - $this->padding,
                $system_colors['color_gap']
            );

            $font_width = imagefontwidth($this->text_size);

            // set text position
            $text_width          = $font_width * strlen($barcode_string);
            $horizontal_centered = floor((($barcode_width - $this->padding) - $text_width) / 2);

            // draw barcode text
            imagestring(
                $image,
                $this->text_size,
                $horizontal_centered,
                ($this->barcode_height - $this->padding) - 12,
                $barcode_string,
                $system_colors['color_bar']
            );
        }

        return $image;
    }
}
