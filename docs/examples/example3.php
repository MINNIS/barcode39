<?php

use MINNIS\Barcode39\Barcode39;

// Normaly one uses the composer autoloader.
// Since this is a stand-alone example inclusing the Barcode39 is sufficient.
include_once __DIR__ . '/../../src/Barcode39.php';

/**
 * Example 3: Output a JPG code 39 barcode image with specific settings
 *
 * The static functions are easy to use but can only default settings.
 * If you want to use custom settings you need to object itself to do so.
 */

$barcode39 = new Barcode39('example3');
$barcode39->padding = 20;
$barcode39->barcode_height = 250;
$barcode39->text_size = 2;
$barcode39->print_startstop_symbol = true;

$barcode39->draw(Barcode39::IMAGE_JPG);
