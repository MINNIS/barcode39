<?php

use MINNIS\Barcode39\Barcode39;

// Normaly one uses the composer autoloader.
// Since this is a stand-alone example inclusing the Barcode39 is sufficient.
include_once __DIR__ . '/../../src/Barcode39.php';

/**
 * Example 4: Get a base64 encode string of a GIF code 39 barcode image
 */

$base64string = Barcode39::base64PNG('example4');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Example 4 - Barcode Code_39 Generator</title>
  </head>
  <body>
    <img src="data:image/png;base64, <?php echo $base64string; ?>" width="300" alt="barcode: EXAMPLE4" title="Look at me, I'm a barcode!" />
  </body>
</html>
