<?php

use MINNIS\Barcode39\Barcode39;

// Normaly one uses the composer autoloader.
// Since this is a stand-alone example inclusing the Barcode39 is sufficient.
include_once __DIR__ . '/../../src/Barcode39.php';

/**
 * Example 2: Output a PNG code 39 barcode image. The same as example 1 but now using the static function
 */

Barcode39::drawPNG('example2');
