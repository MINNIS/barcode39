<?php

use MINNIS\Barcode39\Barcode39;

// Normaly one uses the composer autoloader.
// Since this is a stand-alone example inclusing the Barcode39 is sufficient.
include_once __DIR__ . '/../../src/Barcode39.php';

/**
 * Example 1: Output a PNG code 39 barcode image
 */

$barcode39 = new Barcode39('example1');
$barcode39->draw(Barcode39::IMAGE_PNG);
