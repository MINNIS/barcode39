# Barcode Code_39 Generator

The Barcode Code_39 Generator provides a lightweight easy-to-use class to **generate Code 39 barcodes** in PHP.

## Install

The package can be installed using [composer](https://getcomposer.org):
> composer require minnis/barcode39


## Quick Start

Generating a barcode is as easy as:
``` php
<?php

use MINNIS\Barcode39\Barcode39;

require_once __DIR__ . '/../vendor/autoload.php';

$barcode39 = new Barcode39('123456789');
$barcode39->draw(Barcode39::IMAGE_PNG);
```
resulting in displaying a barcode in PNG format:

![barcode39](https://gitlab.com/MINNIS/barcode39/raw/master/docs/123456789.png "Barcode39: 123456789")

_This PNG barcode is only 280 bytes in filesize! wow!_


## Image Types

The Barcode Code_39 Generator supports 3 filetypes:
- PNG
- GIF
- JPG

Just add the type as parameter to a function. To use jpeg one can use:

``` php
$barcode39 = new Barcode39('123456789');
$barcode39->draw(Barcode39::IMAGE_JPG);
```

## Functions

The Barcode Code_39 Generator can display generated barcodes, save them to disk or return the barcode as base64 encode string.

``` php
// saving the barcode to file:
$filename = 'barcode_123456789.png';
$barcode39 = new Barcode39('123456789');
$barcode39->file(Barcode39::IMAGE_PNG, $filename);

// returing the barcode as base64 encoded string:
$barcode39 = new Barcode39('123456789');
$encodedString = $barcode39->base64(Barcode39::IMAGE_PNG);
```
_the draw and file function return 'true' on succes or 'false' on failure. The base64 function obviously returns a string when succesfull or 'false' when not._

## Shortcuts (statics)

For even more easy usage the Barcode Code_39 Generator provides several preconfigured static functions. 
This allows you to write true one-liners in your code:

``` php
// get a base64 encoded string of a PNG barcode:
$encodedString = Barcode39::base64PNG('123456789');

// save a GIF barcode to file:
Barcode39::fileGIF('123456789', barcode_123456789.gif');

// or output a JPG generated barcode to the browser:
Barcode39::drawJPG('123456789');
```

## Options
### start/end symbols
The Code 39 standard required to start and end a code with a start/end symbol: *

By default the Barcode Code_39 Generator adds those characters automaticly. If, for whatever reason, you want to disable this, you can set the 'use_startstop_symbol' to false:
``` php
$barcode39 = new Barcode39('123456789');
$barcode39->use_startstop_symbol = false;
$barcode39->draw();
```

### barcode height
The barcode height is set by a variable. The default is 100. To change:
``` php
$barcode39 = new Barcode39('123456789');
$barcode39->barcode_height = 200; //pixels
$barcode39->draw();
```

### barcode width
The barcode width is dynamic and varies by the length of the code and used characters. The Code 39 standard uses 2 types of lines (thicks and thins) in a 3:1 ratio. To ensure this ratio one can set a scaling factor:
``` php
$barcode39 = new Barcode39('987654321');
$barcode39->barcode_height = 50;
$barcode39->scale = 3;
$barcode39->draw();
```
Resulting in:

![barcode39](https://gitlab.com/MINNIS/barcode39/raw/master/docs/987654321.png "Barcode39: 987654321")

### footer code
The code is written below the barcode by default. This can be disabled by the use_text variable:
``` php
$barcode39 = new Barcode39('987654321');
$barcode39->use_text = false
$barcode39->draw();
```

### text size
The default fontsize for the code below the barcode is '4'. To change alter the text_size variable:
``` php
$barcode39 = new Barcode39('987654321');
$barcode39->text_size = 6;
$barcode39->draw();
```

## Examples
Check out the examples in **./docs/examples/**
